#!/bin/sh
export WOODPECKER_SERVER="${PLUGIN_WOODPECKER_SERVER}"
export WOODPECKER_TOKEN="${PLUGIN_WOODPECKER_TOKEN}"

#woodpecker-cli -s="${PLUGIN_WOODPECKER_SERVER}" -t="${PLUGIN_WOODPECKER_TOKEN}" info

woodpecker-cli build ls JonathanTreffler/infrastructure --format="number: {{ .Number }}, status: {{ .Status }}, event: {{ .Event }}, commit: {{ .Commit }}, branch: {{ .Branch }}, ref: {{ .Ref }}, message: {{ .Message }}, Author: {{ .Author }}"
