FROM woodpeckerci/woodpecker-cli:next-alpine
COPY entrypoint.sh /bin/
RUN chmod +x /bin/entrypoint.sh
ENTRYPOINT /bin/entrypoint.sh